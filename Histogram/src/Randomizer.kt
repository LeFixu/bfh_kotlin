import kotlin.math.sqrt

object Randomizer
{

    private const val LB = 1000;
    private const val UB = 9999;

    fun pseudoRandomNeumann(seed : Int, lb : Int, ub : Int, depth : Int) : Int {

        require(seed >= LB) { "Seed must be > 1000" }

        require(seed <= sqrt(Int.MAX_VALUE.toDouble())) { "Seed must be smaller than sqrt(Int.MAX_VALUE)" }

        require(lb <= ub) { "lb must be < ub!" }

        require(depth >= 1) { "depth must be > 0" }

        var retVal = seed;
        for ( i in 0..depth) {
            retVal = getNewSeed(retVal);
        }

        return retVal;
    }

    fun getNewSeed(oldSeed : Int) : Int {

        val sq = oldSeed * oldSeed;
        val sqStr = sq.toString();
        val sqStrLen = sqStr.count();

        val startIndex = (sqStrLen - 4) / 2;
        val endIndex = startIndex + 4;


        val newSeedStr = sqStr.subSequence(startIndex, endIndex).toString();

        val retVal = newSeedStr.toInt();
        if (retVal*retVal<1000)
        {
            return retVal + 1000;
        }

        return retVal;
    }

    fun scale(input : Int, lb : Int, ub : Int) : Int {
        return ((input/(UB-LB)).toDouble()*(ub-lb)+ub).toInt();
    }
}