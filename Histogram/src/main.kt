import kotlin.random.Random

fun main() : Unit
{
    val testList1 = List(100) {Random.nextInt(0, 20)}
    val histogram1 = Histogram(testList1)
    histogram1.downward()

    readLine()


    var testList2 = listOf<Int>()
    for (i in 0..3)
    {
        testList2 += Randomizer.pseudoRandomNeumann(i * 10 + 1000, 1000, 10000, 2)
    }

    val histogram2 = Histogram(testList2)
    histogram2.downward()

    readLine()
}