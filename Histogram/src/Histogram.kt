import kotlin.math.pow

class Histogram(list: List<Int>) {
    private val _lb : Int
    private val _ub : Int
    private val _sortedList : List<Int>

    private val _keys : IntRange
    private val _values : IntArray

    init {
        require(list.any()) { "List must not be empty!" }
        _sortedList = list.sorted().distinct()
        _lb = _sortedList.min()!!
        _ub = _sortedList.max()!!
        _keys = _lb.._ub
        _values = IntArray(_keys.count())
        for( curVal in list)
        {
            val index = _keys.indexOf(curVal)
            _values[index]++
        }
    }

    fun downward() {
        println()

        val columnWidth = getStrWidth(_ub)
        printLegend(columnWidth)

        println()

        val maxHeight = _values.max()!!

        for (curHeight in 0..maxHeight)
        {
            printLine(curHeight + 1, columnWidth)
            println()
        }
    }

    private fun printLegend(columnWidth: Int) {
        for (nr in _keys)
        {
            print("$nr")

            val nrWidth = getStrWidth(nr)
            printWhitespaces(columnWidth-nrWidth + 1)
        }
    }

    private fun printLine(curHeight : Int, columnWidth : Int)
    {
        for (curVal in _values)
        {
            if (curVal < curHeight)
            {
                print(".")
            } else {
                print("|")
            }

            printWhitespaces(columnWidth)
        }
    }

    private fun getStrWidth(nr : Int) : Int
    {

        if (nr < 1) {
            return 1
        }

        var digits = 0

        for (i in 0..10)
        {
            if (nr < 10.toDouble().pow(i.toDouble()))
            {
                digits = i
                break
            }
        }

        return digits
    }

    private fun printWhitespaces(amount : Int) {
        for (i in 1..amount)
        {
            print(" ")
        }
    }
}
