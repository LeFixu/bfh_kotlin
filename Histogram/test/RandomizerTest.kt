import org.junit.Test
import kotlin.math.exp
import kotlin.math.sqrt
import kotlin.test.assertFailsWith

class RandomizerTest {

    @Test
    fun makeRandoms() {
        for (i in 1000..9999) {
            val lb = -5;
            val ub = 32450;

            println((i - 1000).toString() + "). Random: " + Randomizer.pseudoRandomNeumann(i, lb, ub, i%7 + 1));
        }


    }

    @Test
    fun pseudoRandomNeumannTestBoundaries() {

        // arrange
        val seed = 3456;
        val lb = 103;
        val ub = 150;
        val depth = 20;

        // act
        val pseudoRand = Randomizer.pseudoRandomNeumann(seed, lb, ub, depth);

        // assert
        // assert(pseudoRand <= ub);
        // assert(pseudoRand >= lb);
    }

    @Test
    fun pseudoRandomNeumanTestArgumentSeedLb() : Unit{
        val block : () -> Unit = { Randomizer.pseudoRandomNeumann(0, 0, 1, 1) };
        assertFailsWith<IllegalArgumentException>(block = block);
    }

    @Test
    fun pseudoRandomNeumanTestArgumentSeedU1() : Unit{
        val block : () -> Unit = { Randomizer.pseudoRandomNeumann(Int.MAX_VALUE, 0, 1, 1 )};
        assertFailsWith<IllegalArgumentException>(block = block);
    }

    @Test
    fun pseudoRandomNeumanTestArgumentSeedU2() : Unit{
        val block : () -> Unit = { Randomizer.pseudoRandomNeumann((sqrt(Int.MAX_VALUE.toDouble()) + 1).toInt(), 0, 1, 1 )};
        assertFailsWith<IllegalArgumentException>(block = block);
    }

    @Test
    fun pseudoRandomNeumanTestArgumentBounds() : Unit{
        val block : () -> Unit = { Randomizer.pseudoRandomNeumann(1500, 2, 1, 1) };
        assertFailsWith<IllegalArgumentException>(block = block);
    }

    @Test
    fun pseudoRandomNeumanTestDepth() : Unit{
        val block : () -> Unit = { Randomizer.pseudoRandomNeumann(1500, 1, 2, 0) };
        assertFailsWith<IllegalArgumentException>(block = block);
    }

    @Test
    fun getNewSeedTestUneven() {
        // arrange
        val oldSeed = 1237; // 1237^2 = 1,530,169‬
        val expectedResult = 5301;

        // act
        val newSeed = Randomizer.getNewSeed(oldSeed);

        // assert
        assert(newSeed == expectedResult) {"Expected $expectedResult, but got $newSeed"};
    }

    @Test
    fun getNewSeedTestEven() {
        // arrange
        val oldSeed = 5678; // 5678^2 = 32,239,684‬
        val expectedResult = 2396;

        // act
        val newSeed = Randomizer.getNewSeed(oldSeed);

        // assert
        assert(newSeed == expectedResult) {"Expected $expectedResult, but got $newSeed"};
    }

    @Test
    fun scaleTest()
    {
        // arrange
        val lb = 5;
        val ub = 78;
        val input = 3459;

        // act
        val retVal = Randomizer.scale(input, lb, ub);

        // assert
        assert(retVal == input/9999*(ub-lb)+ub);
    }
}