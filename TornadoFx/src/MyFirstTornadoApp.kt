import controls.Menus
import javafx.geometry.Pos
import javafx.stage.Stage
import tornadofx.*

class MyFirstTornadoApp : App(MainView::class)
{
    override fun start(stage: Stage) {
        stage.width = 300.0
        stage.height = 150.0
        super.start(stage)
    }
}

class MainView : View("MyFirstTornadoApp")
{ override val root = hbox(alignment = Pos.CENTER){
    label("My first Tornado app!!!")
}}

fun main (args: Array<String>)
{
    //launch<MyFirstTornadoApp>(args)
    //launch<TwoRectangles>(args)
    launch<Menus>(args)
}