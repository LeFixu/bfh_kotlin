/** Programming 1 with Kotlin - HS 19/20,
 *  Computer Science, Bern University of Applied Sciences */

import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.stage.Stage
import tornadofx.*

class TwoRectangles : App(RectView::class) {
    override fun start(stage: Stage) {
        stage.width = 300.0; stage.height = 300.0
        super.start(stage)
    }
}

class RectView : View("TwoRectangleApp") {
    override val root = group {
        rectangle(20, 20, 150, 100)
        rectangle(50, 50, 150, 100) {
            fill = Color.DEEPPINK
        }
        text("Two Rectangles") {
            x = 20.0
            y = 200.0
            font = Font("Courier", 20.0)
        }
    }
}