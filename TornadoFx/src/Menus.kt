package controls

/** Programming 1 with Kotlin - HS 19/20,
 *  Computer Science, Bern University of Applied Sciences */

import javafx.geometry.Pos
import javafx.scene.control.MenuItem
import javafx.stage.Stage
import tornadofx.*


class Menus : App(MView::class) {
    override fun start(stage: Stage) {
        stage.width = 400.0
        stage.height = 200.0
        super.start(stage)
    }
}

class MView : View("Menu Demo") {
    override val root = hbox(spacing = 10, alignment = Pos.TOP_LEFT) {
        // menu
        menubar {
            this.isUseSystemMenuBar = true
            menu("File") {
                item("New") {
                    imageview("file:res/New.png") {
                        fitHeight = 30.0
                        fitWidth = 30.0
                    }
                    action { this@MView.printSelection(this) }
                }
                item("Open") {
                    imageview("file:res/Open.png") {
                        fitHeight = 30.0
                        fitWidth = 30.0
                    }
                    action { this@MView.printSelection(this) }
                }
                separator()
                item("Close") {action { this@MView.printSelection(this) }}
            }

            menu("Options") {
                item("Option 1") { action { this@MView.printSelection(this) } }
                item("Option 2") { action { this@MView.printSelection(this) } }
                item("Option 3") { action { this@MView.printSelection(this) } }
            }
            menu("Help") { }
        }
    }

    private fun printSelection(item: MenuItem) {
        println("You chose the menu item <<${item.text}>>!!")
    }

}

fun main(args: Array<String>) {
    launch<Menus>(args)
}