import javafx.beans.property.StringProperty
import javafx.scene.control.Label
import javafx.scene.control.TextField
import tornadofx.*
import java.lang.NumberFormatException

class SumView : View(){
    private var nmbr1 : Double = 0.0
    private var textField1 : TextField by singleAssign()
    private var nmbr2 : Double = 0.0
    private var textField2 : TextField by singleAssign()

    private val result : Double
        get() = nmbr1 + nmbr2
    private var resultLabel : Label by singleAssign()
    override val root = hbox {
        textField1 = textfield {
            text = "0"
            textProperty().addListener{ observalbe, oldvalue, newValue -> value1Changed(newValue)}
        }
        label { text=" + " }
        textField2 = textfield {
            text = "0"
            textProperty().addListener{ observalbe, oldvalue, newValue -> value2Changed(newValue)}
        }
        label { text = " = " }
        resultLabel = label { text = "0"}
    }

    private fun value1Changed(value : String) {
        val conversion = tryParseAsNumber(value)

        if(!conversion.first)
        {
            textField1.text = 0.0.toString()
        }

        nmbr1 = conversion.second
        setResult()
    }

    private fun value2Changed(value : String) {
        val conversion = tryParseAsNumber(value)

        if(!conversion.first)
        {
            textField1.text = 0.0.toString()
        }

        nmbr2 = conversion.second
        setResult()
    }

    private fun setResult(){
        resultLabel.text = result.toString()
    }

    private fun tryParseAsNumber(str : String) : Pair<Boolean, Double> {
        return try {
            val nmbrValue = str.toDouble()
            Pair(true, nmbrValue)
        } catch (e: NumberFormatException){
            Pair(false, 0.0)
        }
    }
}

class SumApp : App(SumView::class)
fun main(args : Array<String>){
    launch<SumApp>(args)
}