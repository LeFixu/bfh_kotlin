import javafx.beans.binding.Bindings
import javafx.scene.control.TextField
import tornadofx.*

class BindingsView : View(){
    var boundTo1 : TextField by singleAssign()
    var boundBiDirectional : TextField by singleAssign()
    var concat1 : TextField by singleAssign()
    var concat2 : TextField by singleAssign()
    var functional1 : TextField by singleAssign()
    var functional2 : TextField by singleAssign()
    override val root = vbox {
        hbox {
            boundTo1 = textfield { text = "Unidirectional" }
            textfield {
                bind(boundTo1.textProperty())
                isDisable = true
            }
        }
        hbox {
            boundBiDirectional = textfield("Bidirectional")
            textfield {
                textProperty().bindBidirectional(boundBiDirectional.textProperty())
            }
        }
        hbox {
            concat1 = textfield ("Concat1")
            concat2 = textfield ("Concat2")
            textfield {
                bind(Bindings.concat(concat1.textProperty(), " ", concat2.textProperty()))
                isDisable = true
            }
        }
        hbox {
            functional1 = textfield ("Functional") {
                textProperty().addListener{ observable, oldValue, newValue ->
                    println("Functional text field has changed from $oldValue to $newValue")
                    functional2.textProperty().set(observable.value)
                }
            }
            functional2 = textfield ("binding")
            textfield {
                isDisable = true
            }
        }
    }
}

class BindingsApp : App(BindingsView::class)
fun main (args:Array<String>){
    launch<BindingsApp>(args)
}