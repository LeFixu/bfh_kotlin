import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TableView.CONSTRAINED_RESIZE_POLICY
import javafx.scene.layout.Priority
import tornadofx.*
import java.time.LocalDate
import java.time.Period

class ObservableCollectionView : View("Table View (editable)"){
    private val persons = mutableListOf(
            Person(1, "Samantha Stuart", LocalDate.of(1981, 12, 4)),
            Person(2, "Tom Marks", LocalDate.of(2001, 1, 23)),
            Person(3, "Stuart Gills", LocalDate.of(1989, 5, 23)),
            Person(4, "Nicole Williams", LocalDate.of(1998, 8, 11))
    ).asObservable()

    override val root = borderpane {
        center {
            vbox {
                val peopleTable = tableview (persons) {
                    readonlyColumn("ID", Person::id)
                    column("Name", Person::name)
                    readonlyColumn("Birthday", Person::birthday)
                    readonlyColumn("Age", Person::age)
// No empty colums
                    columnResizePolicy = CONSTRAINED_RESIZE_POLICY
// Table fit parent
                    vboxConstraints {
                        vGrow = Priority.ALWAYS
                    }

                    this.enableCellEditing()
                    regainFocusAfterEdit()
                }

                button("Add") {
                    action {
                        persons.add(Person(persons.count() + 1, "Hans Muster", LocalDate.of(2003, 3, 4)))
                    }
                }

                button("Remove") {
                    action {
                        val person = peopleTable.selectedItem
                        if (person != null) {
                            println("Removing: $person")
                            persons.removeIf { it -> it.id == person?.id }
                        } else
                            println("Nothing selected")
                    }
                }

                button("Print") {
                    action {
                        persons.forEach{println(it)}
                    }
                }
            }
        }
    }
}
class Person(val id: Int, _name: String, val birthday: LocalDate) {
    val nameProperty = SimpleStringProperty(this, "name", _name)
    var name: String by nameProperty
    val age: Int get() = Period.between(birthday, LocalDate.now()).years
    override fun toString(): String {
        return "Person(id=$id, birthday=$birthday, name=$name)"
    }
}
class ObservableCollectionApp : App(ObservableCollectionView::class)
fun main (args:Array<String>){
    launch<ObservableCollectionApp>(args)
}