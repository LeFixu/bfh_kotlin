import javafx.scene.layout.AnchorPane
import javafx.stage.Stage
import tornadofx.*
import javafx.scene.control.Label
import java.nio.file.Paths

class TwoButtonsView : View("TwoButtons") {
    override val root: AnchorPane by fxml()
    private val label1 : Label by fxid()
    private val label2 : Label by fxid()

    private var counter1 : Int = 0
    private var counter2 : Int = 0

    fun btn1_click(){
        counter1++
        label1.text = "Nr of clicks: $counter1"
    }

    fun btn2_click(){
        counter2++
        label2.text = "Nr of clicks: $counter2"
    }
}

class TwoButtons : App(TwoButtonsView::class){
    init {
        importStylesheet(Paths.get("./src/main/resources/TwoButtons.css"))
    }

    override fun start(stage : Stage) {
        stage.width = 500.0
        stage.height = 400.0
        super.start(stage)
    }
}

fun main(args: Array<String>){
    launch<TwoButtons>(args)
}