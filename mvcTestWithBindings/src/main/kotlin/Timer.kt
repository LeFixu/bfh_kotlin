import javafx.application.Platform
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.layout.AnchorPane
import tornadofx.App
import tornadofx.View
import tornadofx.launch
import java.util.*

/** Programming 2 with Kotlin - FS 19/20,
 *  Computer Science, Bern University of Applied Sciences */

fun main(args : Array<String>) {
    launch<TimerApp>(args)
}

class TimerApp : App(TimerView::class)

class TimerView : View("Time") {
    override val root: AnchorPane by fxml()
    private val btnStart : Button by fxid()
    private val btnStop : Button by fxid()
    private val timer = Timer(100)
    private val stopWatch = StopWatch(timer)

    init {
        btnStart.isDisable = false
        btnStop.isDisable = true

        root.add(stopWatch)
        stopWatch.reset()
    }

    @Suppress("unused")
    fun start()
    {
        timer.reset()
        timer.start()
        stopWatch.reset()
        btnStart.isDisable = true
        btnStop.isDisable = false
    }

    @Suppress("unused")
    fun stop()
    {
        timer.stop()
        btnStart.isDisable = false
        btnStop.isDisable = true
    }
}

class StopWatch(timer: Timer) : Label(), Observer {

    init {
        timer.addObserver(this)
        reset()
    }

    override fun update(o: Observable?, arg: Any?) {
        val timer = o as Timer
        Platform.runLater { text = timer.time.toString() }
    }

    fun reset()
    {
        text = 0.0.toString()
    }
}

class Timer(private val interval: Int) : Runnable, Observable() {
    private var ticks = 0
    private val factor = 1000.0
    private var thread: Thread? = null
    val time: Double
        get() = ticks * interval / factor

    fun start() {
        if (thread == null) {
            val newthread = Thread(this)
            newthread.isDaemon = true
            newthread.start()
            thread = newthread
        }
    }

    fun stop() {
        if (thread != null) {
            thread = null
        }
    }

    fun reset() {
        ticks = 0
    }

    override fun run() {
        while (thread != null) {
            Thread.sleep(interval.toLong())
            ticks++
            setChanged()
            notifyObservers()
        }
    }
}