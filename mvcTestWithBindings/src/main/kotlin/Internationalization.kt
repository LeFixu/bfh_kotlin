import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.layout.AnchorPane
import tornadofx.App
import tornadofx.FX
import tornadofx.View
import tornadofx.launch
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

@Suppress("unused")
class InternationalizationView : View() {

    private val button : Button by fxid()
    private val label : Label by fxid()
    private val date : Label by fxid()

    // It's important that the localisation comes before the override val root!
    // Otherwise the text from the resource can't be loaded (localisation info not yet there)
    init {
        val chosenLang = app.parameters.named["lang"]
        FX.locale = Locale(chosenLang)
        FX.messages = ResourceBundle.getBundle("ButtonLabel", FX.locale)
    }

    override val root: AnchorPane by fxml()

    init {
        val timeFormatter = DateTimeFormatter
                .ofLocalizedDate(FormatStyle.FULL)
                .withLocale(FX.locale)
        date.text = timeFormatter.format(LocalDate.now())
    }
    fun buttonPressed() {
        label.isVisible = !label.isVisible
    }
}

class InternationalizationApp : App(InternationalizationView::class)

fun main(args : Array<String>) {
    println(Locale.getAvailableLocales().toList())
    launch<InternationalizationApp>(args)
}