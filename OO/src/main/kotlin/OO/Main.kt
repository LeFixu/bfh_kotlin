package OO

fun main() {
    val phone = Phone()
    var curChr = '\n'
    println("Enter your phone number!")
    do {
        val curNullableStr: String? = readLine() ?: continue
        val curStr = curNullableStr.toString()

        curChr = if (curStr.isEmpty()) {
            '\n'
        } else {
            if (curStr == "*backspace*"){
                '\b'
            } else {
                curStr[0]
            }
        }
    } while(phone.addDigit(curChr))
}

