package OO

class Phone {
    private val digits = mutableListOf<Char>()
    private val charDigits = CharRange('0', '9')
    private val charBackspace = '\b'
    private val charEnter = '\n'
    private val minPhoneNrLength = 10

    fun addDigit(chr : Char) : Boolean {

        var isValid = false
        if (charDigits.contains(chr))
        {
            digits += chr
            isValid = true
        }

        if (chr == charBackspace)
        {
            digits.remove(digits.last())
            isValid = true
        }

        if (isValid)
        {
            println()
            println(digits)
            return true
        }

        if (chr == charEnter)
        {
            return dial()
        }

        print("Invalid input)")
        return true
    }

    private fun dial() : Boolean {

        if (digits.count() < minPhoneNrLength){
            print("Enter more digits!")
            return true
        }

        println()
        println("Dialing...")

        return false
    }
}