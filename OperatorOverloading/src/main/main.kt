// operator names https://kotlinlang.org/docs/reference/operator-overloading.html#operator-overloading
// operator precedence list https://kotlinlang.org/docs/reference/grammar.html#expressions
data class A (val amount : Int)
operator fun A.plus(otherA : A) = A(amount + otherA.amount)

fun main() {
    val a1 = A(5)
    val a2 = A(7)
    println(a1)
    println(a2)
    println(a1+a2)
}