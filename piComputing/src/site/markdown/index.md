## Exercise piComputing

### Leibniz Series

The number PI = 3:141592653... can be computed approximately using the Gregory-Leibniz Series:

    PI/4 = 1 - 1/3 + 1/5 - 1/7 + 1/9 - ...

Write a Kotlin function that computes PI with a precision of 3 digits after the decimal point. How many iterative steps are needed for that?

For your solution, you may want to use the following function for rounding a number to k places after the decimal point:

    private fun truncate(n: Double, k: Int): Double {
         val c = (10.0).pow(k)
         return floor(n * c) / c
     }


### Nalakantha Series

Another way to compute PI is to use Nilakantha Series developed in 15th century

    PI = 3 + 4/(2*3*4) - 4/(4*5*6) + 4/(6*7*8) - 4/(8*9*10) + ...

Develop another function similar to the previous one but using the Nilakantha Series.


---

To generate this site, go into the root directory
of the project (where the `pom.xml` file is) and then type:

> `mvn clean site`

You'll find the generated site `index.html` in directory target/site.

To compile and test your source code, still in the root directory, type:

> `mvn clean package`

