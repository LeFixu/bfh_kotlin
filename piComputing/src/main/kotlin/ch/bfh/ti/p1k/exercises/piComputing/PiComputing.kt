package ch.bfh.ti.p1k.exercises.piComputing

import kotlin.math.PI
import kotlin.math.floor
import kotlin.math.pow
import kotlin.math.absoluteValue

/**
 * Truncates a double n after k digits following the decimal point,
 * for example truncate(3.141592, 3) -> 3.141
 *
 * @param n number to truncate
 * @param k number of digits after the decimal point
 */
private fun truncate(n: Double, k: Int): Double {
    val c = (10.0).pow(k.toDouble())
    return floor(n * c) / c
}

/**
 * Computes the approximation of PI using the Leibniz approach.
 * This function is used to observe how the convergence of the series works.
 *
 *     pi/4 = 1 - 1/3 + 1/5 - 1/7 + 1/9 - ...
 *
 * @param maxIter number of iterations max to perform
 * @param nDigits precision in number of digits after the decimal point
 * @return approximation of PI
 */
fun piLeibnizStudy(maxIter: Int, nDigits: Int) : Unit {
    var piApprox4 : Double = 1.0           // approximation of pi/4
    var i = 1                              // ith iteration
    val piNDigits = truncate(PI, nDigits)  // PI truncated to nDigits after decimal point

    do {
        print("iter=$i \t piNDigits=${piNDigits} \t piApprox=${piApprox4 * 4 } \t ")
        print("distance(piApprox, piNDigits)=${piApprox4 * 4-piNDigits} \t ")
        println("distance(pi, piApprox)= ${piApprox4 * 4 - PI}")
        piApprox4 = piApprox4 + (-1.0).pow(i) /(i*2+1).toDouble()
        i++
     } while (i <= maxIter)
}

/**
 * Determines how many iterations are necessary for the computation of PI approximation
 * to guarantee that n digits after the decimal point are correct.
 * What we have to guarantee is that n digits after the decimal point are correct for ALL
 * the further iterations.
 *
 * @param nDigits number of digits after the decimal point that are guaranteed
  */
fun piLeibnizWithNDigits(nDigits: Int) : Int {
    var piApprox4 = 1.0                   // approximation of pi/4
    var previousPiApprox4 : Double        // approximation of the previous iteration
    var i = 1                             // ith iteration
    val epsilon = 10E-10                  // to compare Double and avoid rounding error

    val piNDigits = truncate(PI, nDigits)
    do {
        previousPiApprox4 = piApprox4     // previous approximation is now the current one
        piApprox4 = piApprox4 + (-1.0).pow(i) / (i * 2 + 1).toDouble()
        i++                               // do-while stops when both approximations are the same
    } while (((truncate(previousPiApprox4 * 4, nDigits) - piNDigits).absoluteValue > epsilon) ||
             ((truncate(piApprox4 * 4, nDigits) - piNDigits).absoluteValue > epsilon))

    return i
}

/**
 * Computes the approximation of PI using the Nilakantha approach.
 * This function is used to observe how the convergence of the series works.
 *
 *    PI = 3 + 4/(2*3*4) - 4/(4*5*6) + 4/(6*7*8) - 4/(8*9*10) + ...
 *
 * @param maxIter number of iterations max to perform
 * @param nDigits precision in number of digits after the decimal point
 * @return approximation of PI
 */
fun piNilakanthaStudy(maxIter: Int, nDigits: Int) : Unit{
    var piApprox: Double = 3.0           // approximation of pi/4
    var i = 1                              // ith iteration
    val piNDigits = truncate(PI, nDigits)  // PI truncated to nDigits after decimal point

    do {
        print("iter=$i \t piNDigits=${piNDigits} \t piApprox=${piApprox} \t ")
        print("distance(piApprox, piNDigits)=${piApprox - piNDigits} \t ")
        println("distance(pi, piApprox)= ${piApprox - PI}")
        piApprox = piApprox + (-1.0).pow(i + 1) * 4 / ((2 * i) * (2 * i + 1) * (2 * i + 2))
        i++
    } while (i <= maxIter)
}


fun piNilakanthaWithNDigits(nDigits: Int) : Int {
    var piApprox = 3.0                    // approximation of pi
    var previousPiApprox : Double         // approximation of the previous iteration
    var i = 1                             // ith iteration
    val epsilon = 10E-10                  // to compare Double and avoid rounding error
    val piNDigits = truncate(PI, nDigits)

    do {
        previousPiApprox = piApprox       // previous approximation is now the current one
        piApprox = piApprox + (-1.0).pow(i+1) * 4 / ((2*i) * (2*i+1) * (2*i+2))
        i++
    } while (((truncate(previousPiApprox, nDigits) - piNDigits).absoluteValue > epsilon) ||
             ((truncate(piApprox, nDigits) - piNDigits).absoluteValue > epsilon))

    return i
}

fun main() {
    println("PI Leibniz")

    // Study the Leibniz series
//  piLeibnizStudy(2500, 3)

    println()

    // Determine the number of iterations for a given number of digits after the decimal point
    println("Number of iterations = ${piLeibnizWithNDigits(3)}")

    println()

    println("PI Nilakantha")

    // Study the Nilakantha serie
//    piNilakanthaStudy(50, 5)

    println()

    // Determine the number of iterations for a given number of digits after the decimal point
    println("Number of iterations = ${piNilakanthaWithNDigits(3)}")

}
