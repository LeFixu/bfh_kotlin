fun printValues(values: Array<*>) {
    for (value in values)
        println(value)
//values[0] = values[1] CTE: type mismatch
}

fun <T : Fruit> useAndPeel1(fruits: Array<T>) {
    for (f in fruits)
        f.peel()
}

fun useAndPeel2(fruits: Array<out Fruit>) {
    for (f in fruits)
        f.peel()
}

fun <T> useAndPeel3(fruits: Array<T>) where T:Fruit /*, T:OtherInterface*/ {
    for (f in fruits)
        f.peel()
}