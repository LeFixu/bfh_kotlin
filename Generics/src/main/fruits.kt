open class Fruit(val fruitType : String = "fruit") {
    fun peel(){}
    override fun toString(): String {
        return fruitType
    }
}
class Banana:Fruit("banana")
class Orange:Fruit("orange")