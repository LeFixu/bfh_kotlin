fun receiveFruits1(fruits: Array<out Fruit>) {
//    if (fruits is Array<Banana>) { // CTE: Cannot check for instance of erased type: Array<Banana>
//        println("type is banana")
//    }
}

inline fun <reified T : Fruit> fruitsAreT(fruits: Array<out Fruit>) {
    if (fruits is T) { // CTE: Cannot check for instance of erased type: Array<Banana>
        println("type is T")
    }
}