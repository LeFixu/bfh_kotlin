fun <T : Fruit> copyFromTo(from: Array<out T>, to: Array<in T>) {
    for (i in 0 until from.size)
        to[i] = from[i]
}
fun main() {
    val fruitBasket = Array<Fruit>(3) {Fruit()}
    val bananaBasket = Array<Banana>(3) {Banana()}
    copyFromTo(bananaBasket,fruitBasket)
    fruitBasket.forEach { println(it) }
}