import java.util.concurrent.Executors

fun threadPool(poolSize : Int, n : Int, action : () -> Unit) {
    val executors = Executors.newFixedThreadPool(poolSize)
    repeat(n){executors.submit{action()}}
    executors.shutdown()
}