import kotlinx.coroutines.Dispatchers
import java.time.LocalDateTime
import kotlin.system.measureTimeMillis

fun main() {
//    val n = 10
//    timerWrapper { threadSingle { println(".") } }
//    println()
//    timerWrapper { threadMany(n){print(".")} }
//    println()
//    timerWrapper { threadPool(1024, n) { task1() } }
//    println()
//    timerWrapper { coroutineRunTwo({task1()}, {task2()}) }
//    println()
//    timerWrapper { coroutineRunTwoNice() }
//    println()
//    timerWrapper { coroutineRunTwoNice(Dispatchers.IO) } // for IO operations
//    println()
//    timerWrapper { coroutineInCustomThreadPool() }
//    println()
//    timerWrapper { println("AvailableCores: " + coroutineWithReturnValue()) }
//    println()
//    timerWrapper { println(getAirportDataString("JFK")) }
//    println()
    timerWrapper {
        val airports = getAirportsAsync(listOf("JFK", "LAX", "SFO", "PDX", "ANC", "FLG", "BUR", "ASE", "JAX", "Erroneous"))
        printAirportTable(airports)
    }
}

fun timerWrapper(action : () -> Unit) {
    val millis = measureTimeMillis(action)
    println("Time for execution: " + millis + "ms")
}