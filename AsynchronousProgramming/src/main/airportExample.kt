import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.net.URL

fun getAirportDataString(airportCode : String) : String {
    return URL("https://soa.smext.faa.gov/asws/api/airport/status/"+ airportCode).readText()
}

fun getAirportsAsync(airportCodes : Collection<String>) : Collection<Airport> {
    val retVal = mutableListOf<Airport>()
    runBlocking {
        val deferreds = airportCodes.map {code ->
            async(Dispatchers.Default + SupervisorJob()){
                println("Starting fetching airport: $code; Thread: " + Thread.currentThread().toString())
                val airport =  getAirport(code)
                println("Finished fetching airport: $code; Thread: " + Thread.currentThread().toString())
                airport
            }
        }

        for (d in deferreds) {
            try {
                val airport = d.await()
                retVal.add(airport)
            } catch (ex : Exception) {
                println("Failed to fetch code!")
                println(ex.message?.substring(0..50))
            }
        }
    }
    return retVal
}

fun getAirport(airportCode: String) : Airport {
    val jsonString = getAirportDataString(airportCode)
    val mapper = jacksonObjectMapper()
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    return mapper.readValue(jsonString)
}
fun printAirportTable(airports : Collection<Airport>) {
    val format = "%-10s%-20s%-10s%-20s"
    println(String.format(format, "Code", "City", "Delay", "Weather"))
    airports.forEach{a -> println(String.format(format,a.code, a.city, a.delay, a.weather.temp[0]))}
}

data class Airport(
        @JsonProperty("IATA")
        val code : String,
        @JsonProperty("Name")
        val name : String,
        @JsonProperty("City")
        val city : String,
        @JsonProperty("Delay")
        val delay : Boolean,
        @JsonProperty("Weather")
        val weather: Weather,
        @JsonProperty("Status")
        val status : AirportStatus
)

data class Weather(
    @JsonProperty("Temp")
    val temp : Array<String>)

data class AirportStatus(
    @JsonProperty("Reason")
    val reason : String)