import kotlinx.coroutines.*
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext

fun coroutineWithReturnValue() : Int {
    var retVal = 0
    runBlocking {
        retVal = async (Dispatchers.Default) {
            Runtime.getRuntime().availableProcessors()
        }.await()
    }
    return retVal
}

fun coroutineInCustomThreadPool() {
    Executors.newSingleThreadExecutor().asCoroutineDispatcher().use{
        runBlocking {
            launch { task1Nice() }
            launch { task2Nice() }
        }
    }
    Executors.newFixedThreadPool(20).asCoroutineDispatcher().use{ context ->
        runBlocking(context) {
            repeat(100){
            launch { task1Nice() }
            launch { task2Nice() }
        }}
    }
}

fun coroutineRunTwoNice(context : CoroutineContext = Dispatchers.Default) {
    runBlocking {
//    runBlocking(context) { // also possible
        launch(context) { task1Nice()}
        launch(context) { task2Nice()}
        println("called task1 and task2 from Thread ${Thread.currentThread()}")
    }
}

fun coroutineRunTwo(action1 : () -> Unit, action2: () -> Unit) {
    runBlocking {
        launch { action1()}
        launch {action2()}
        println("called task1 and task2 from Thread ${Thread.currentThread()}")
    }
}
fun task1() {
    println("Start task1 in Thread ${Thread.currentThread()}")
    println("End task1 in Thread ${Thread.currentThread()}")
}
fun task2() {
    println("Start task2 in Thread ${Thread.currentThread()}")
    println("End task2 in Thread ${Thread.currentThread()}")
}
private suspend fun task1Nice() {
    println("Start task1 in Thread ${Thread.currentThread()}")
    yield() // alternatively use delay(timeInMilliSeconds)
    println("End task1 in Thread ${Thread.currentThread()}")
}
private suspend fun task2Nice() {
    println("Start task2 in Thread ${Thread.currentThread()}")
    yield()
    println("End task2 in Thread ${Thread.currentThread()}")
}