import kotlin.concurrent.thread

fun threadSingle(action : () -> Unit) {
    thread{action()}
}

fun threadMany(n : Int, action : () -> Unit) {
    val lst = List(n){action}
    lst.forEach { it() }
}