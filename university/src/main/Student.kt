import java.util.*

class Student
{
    var id : String = ""
        private set

    init {
        id = UUID.randomUUID().toString()
    }
    
    constructor(grade : Int) {
        this.grade = grade
    }

    var grade : Int = 0
        set(value) {
            require(value in 0..10)
            field = value
        }

    override fun toString() : String {
        return "$id with grade $grade (1-10)"
    }
}