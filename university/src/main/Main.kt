fun main(){
    val student1 = Student(0)
    val student2 = Student(4)
    val student3 = Student(7)
    val student4 = Student(10)
    val student5 = Student(8)
    val student6 = Student(2)
    val student7 = Student(4)
    val student8 = Student(4)
    val student9 = Student(1)

    val course = Course("Biberstein")
    course.addStudent(student1)
    course.addStudent(student2)
    course.addStudent(student3)
    course.addStudent(student4)
    course.addStudent(student5)
    course.addStudent(student6)
    course.addStudent(student7)
    course.addStudent(student8)
    course.addStudent(student9)

    println(course)
    println()

    course.removeStudent(student1)
    course.removeStudent(student3)
    course.removeStudent(student2)

    println(course)
    println()
}