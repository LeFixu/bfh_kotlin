import java.lang.IllegalArgumentException
import java.util.*
import kotlin.collections.ArrayList

class Course {

    var id: String = ""
        private set

    init{
        id = UUID.randomUUID().toString()
    }

    constructor(lecturer : String) {
        this.lecturer = lecturer
    }

    var lecturer: String = ""

    private var students: ArrayList<Student> = ArrayList()

    fun addStudent(student: Student) {
        require(student !in students)
        students.add(student)
    }

    fun removeStudent(student: Student) {
        require(student in students)
        students.remove(student)
    }

    fun numberOfStudents() = students.size
    fun avgGrade(): Double = students.sumBy { s -> s.grade }.toDouble() / numberOfStudents()

    override fun toString(): String {
        var retVal = "$id lectured by $lecturer with students:"

        for (st in students)
        {
            retVal += "\n   -$st"
        }

        retVal += "\nAverage grade: " + avgGrade()

        return retVal
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Course

        if (id != other.id) return false
        if (lecturer != other.lecturer) return false
        if (students != other.students) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + lecturer.hashCode()
        result = 31 * result + students.hashCode()
        return result
    }
}