import com.sun.xml.internal.fastinfoset.util.StringArray
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage
import tornadofx.*
import java.nio.file.Paths
import java.time.Duration
import java.util.*

/**
 * Class acts like controller.
 * A true controller is declared implicitly by TornadoFX to link the view and the .fxml file
 */
class MVCExampleView : View(), Observer
{
    private val txtText : TextField by fxid()
    private val txtRepetitions : TextField by fxid()
    private val lstList : ListView<String> by fxid()
    private val lblDoomsdayClock : Label by fxid()
    private val selLanguage : ChoiceBox<String> by fxid()
    private val ddC = DoomsdayClock()

    init {
        setLanguage()
        ddC.addObserver(this)
    }

    // Gotta be after init to ensure translations are loaded
    override val root : AnchorPane by fxml()

    @Suppress("unused")
    fun submit() {
        val txt = txtText.text
        var repetitions : Int by singleAssign()
        try {
            repetitions = txtRepetitions.text.toInt()
        } catch(nfe : NumberFormatException) {
            println(nfe.message)
            txtRepetitions.addClass("erroneous")
            return
        }
        txtRepetitions.removeClass("erroneous")

        for (i in 0 until repetitions)
        {
            lstList.items.add(txt)
        }
    }

    @Suppress("unused")
    fun reset() {
        lstList.items.clear()
    }

    private fun setLanguage(){
        val chosenLang = app.parameters.named["lang"] // startup parameter --lang=de_CH
        FX.locale = if (chosenLang == null) {
            Locale.ROOT
        }
        else {
            Locale(chosenLang)
        }
        FX.messages = ResourceBundle.getBundle("MVCExampleView", FX.locale)
    }

    override fun update(o: Observable?, arg: Any?) {
        println("Current time: " + System.currentTimeMillis())
        val ddC = o as DoomsdayClock
        println(ddC.timeUntilDoom.toString())

        // important for thread safety
        Platform.runLater { lblDoomsdayClock.text = ddC.toString()}
    }
}
class MVCExample : App(MVCExampleView::class){
    init {
        importStylesheet(Paths.get("./src/resources/MVCExampleView.css"))
        // add more style sheets
    }
    override fun start(stage: Stage) {
        // main theme
        setUserAgentStylesheet(STYLESHEET_MODENA)
        super.start(stage)
    }
}

fun main(args: Array<String>){
    println(Locale.getAvailableLocales().toList())
    launch<MVCExample>(args)
}