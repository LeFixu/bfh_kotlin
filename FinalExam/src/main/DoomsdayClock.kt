import java.util.*
import kotlin.math.floor
import kotlin.math.roundToInt

class DoomsdayClock : Observable(), Runnable {
    var timeUntilDoom : Long = 0L
        private set
    private val doomTime = Calendar.getInstance()

    init {
        val t = Thread(this)
        t.isDaemon = true
        t.start()
    }

    override fun run() {
        doomTime.set(2020,5,29,16,0)
        println("Doomtime: " + doomTime.timeInMillis)
        while(true) {
            timeUntilDoom = doomTime.timeInMillis - System.currentTimeMillis()
            setChanged()
            notifyObservers()
            Thread.sleep(1000)
        }
    }

    override fun toString() : String {
        val dayInMillis = 1000 * 3600 * 24
        val hourInMillis = 3600000
        val minuteInMillis = 60000
        val days = floor( timeUntilDoom.toDouble() / dayInMillis)
        val millisWoDays = timeUntilDoom - days * dayInMillis
        val hours = floor(millisWoDays / hourInMillis)
        val millisWoDaysAndHours = millisWoDays - hours * hourInMillis
        val minutes = floor(millisWoDaysAndHours / minuteInMillis)
        val millisWoDaysHoursAndMinutes = millisWoDaysAndHours - minutes * minuteInMillis
        val seconds = floor(millisWoDaysHoursAndMinutes / 1000)

        return days.roundToInt().toString() + "d " + hours.roundToInt().toString() + "h " + minutes.roundToInt().toString() + "m " + seconds.roundToInt().toString() + "s"
    }
}