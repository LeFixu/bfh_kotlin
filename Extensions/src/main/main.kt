class A

fun A.aExt(){println("ExtFunction")}
var A.extProperty : String
    get() = "ExtProperty"
    set(value) {}

fun main() {
    val a = A()
    a.aExt()
    println(a.extProperty)
}