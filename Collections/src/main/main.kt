fun main() {
    val mutableList = mutableListOf<Int>()
    mutableList.add(1)
    mutableList.add(2)
    println(mutableList)

    val immutableList = listOf<Int>(1)
//    immutableList.add(2) // CTE no such method
    println(immutableList)

    val immutableList2 = List(4) {it*it}
    println(immutableList2)

    val set1 = setOf(1, 2, 3)
    println(set1)
    val set2 = set1.plus(1) // element not added because duplicate
    println(set2)

    val map = mapOf(1 to 'a', 2 to 'b')
    println(map)
    val map2 = mapOf(Pair(1,'a'), Pair(2, 'b'))
    println(map2)

    // use iterator
    println()
    println("Print list using iterator")
    val it = mutableList.iterator()
    while(it.hasNext())
    {
        println(it.next())
    }
}