fun factorialLinear(n: Int) : Long {
    return if (n == 0) 1
    else n * factorialLinear(n - 1)
}

tailrec fun factorialTailrec(n: Int, curResult: Long = 1) : Long {
    return if (n == 0){
        curResult
    } else {
        return factorialTailrec(n-1, curResult * n)
    }
}

fun isOdd(n : Int) : Boolean {
    return if (n == 0) false else isEven(n-1)
}
fun isEven(n : Int) : Boolean {
    return if (n == 0) true else isOdd(n-1)
}

fun mrAckermann(m : Long, n : Long) : Long {
    if (n < 0 || m < 0) {
        return 0
    }
    if (m == 0L){
        return n + 1
    }
    if (m > 0 && n == 0L) {
        return mrAckermann(m-1,1)
    }
    return mrAckermann(m-1, mrAckermann(m, n-1))
}

fun main(){
    val n = 25
    println("Linear")
    println(factorialLinear(n))
    println()
    println("Tailrec")
    println(factorialTailrec(23))

    println()
    println("25 is even: " + isEven(25))
    println()

    val limit = 3
    for (m in 0..limit)
    {
        for (n in 0..limit)
        {
            println("m: " + m + "; n: " + n + "; Ackermann: " + mrAckermann(m.toLong(), n.toLong()))
        }
    }
}