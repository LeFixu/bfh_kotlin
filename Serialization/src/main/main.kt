import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.*
import kotlin.collections.ArrayList

fun main(){
    val people = getPeople()

    println("Data objects")
    people.forEach{println(it)}

    println()
    xmlIO(people)

    println()
    jsonIO(people)
}

fun xmlIO(people: ArrayList<Person>){
    val xmlMapper = XmlMapper()
    xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true)

    val xmlString = xmlMapper.writeValueAsString(people)
    println("Xml string")
    println(xmlString)

    val xmlWriter = FileWriter("people.xml")
    xmlMapper.writeValue(xmlWriter, people)

    val peopleFromFile = xmlMapper.readValue<ArrayList<Person>>(FileInputStream("people.xml"))
    println()
    println("From Xml file")
    peopleFromFile.forEach{println(it)}
}

fun jsonIO(people : ArrayList<Person>) {
    val jsonMapper = jacksonObjectMapper()
    val jsonString = jsonMapper.writeValueAsString(people)
    println("Json string")
    println(jsonString)

    val jsonWriter = FileWriter("people.json")
    jsonMapper.writeValue(jsonWriter, people)

    val outputStreamWriter = OutputStreamWriter(FileOutputStream("peopleEncoded.json"), "UTF-16")
    jsonMapper.writeValue(outputStreamWriter, people)

    println()
    println("Start reading files...")
    println()

    println("Default reading technique")
    val jsonReader = FileInputStream("people.json")
    val peopleFromJson = jsonMapper.readValue<ArrayList<Person>>(jsonReader)
    peopleFromJson.forEach{println(it)}

    println("Specify encoding")
    val peopleFromEncodedJson = jsonMapper.readValue<ArrayList<Person>>(InputStreamReader(FileInputStream("peopleEncoded.json"), "UTF-16"))
    peopleFromEncodedJson.forEach { println(it) }
}

fun getPeople() : ArrayList<Person> {
    val comments1 = ArrayList<Comment>()
    comments1.add(Comment("Just now", "2020-06-24"))
    comments1.add(Comment("Yesterday", "2020-06-23"))
    val person1 = Person("1234", "Peter Fox", comments1)

    val comments2 = ArrayList<Comment>()
    comments2.add(Comment("Some nice thing to say", "2020-06-01"))
    comments2.add(Comment("Some bad thing to say", "2020-06-13"))
    comments2.add(Comment("Test them umlaute äöüéèà", "2020-06-24"))
    comments2.add(comments1.first())
    val person2 = Person("1000", "Jack White", comments2)

    val people = ArrayList<Person>()
    people.add(person1)
    people.add(person2)

    return people
}

data class Comment(val comment : String = "", val date : String = "")
data class Person(val ssn : String = "", val name : String = "", val comments : ArrayList<Comment> = ArrayList<Comment>())