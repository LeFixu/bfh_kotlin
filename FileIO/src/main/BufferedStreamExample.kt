import java.io.*

/**
 * BufferedFileStream
 */
fun bufferedFileStream(file1: String, file2: String) {
    // binary streams
    val istream = BufferedInputStream(FileInputStream(file1)) // all streams possible to insert
    val ostream = BufferedOutputStream(FileOutputStream(file2))
    // character streams
    val reader = BufferedReader(FileReader(file1))
    val writer = BufferedWriter(FileWriter(file2))
}