fun main() {
    val account1 = BankAccount(1, "John Doe", 50000.0, 0.05f)
    val account2 = BankAccount(2, "Max Muster", 27934.0, 0.07f)
    val accounts = ArrayList<BankAccount>()
    accounts.add(account1)
    accounts.add(account2)
    accounts.forEach { println(it) }
    println()
    writeBankData("bankAccounts.data", accounts)
    println()
    val accountsFromFile = readBankData("bankAccounts.data")
    accountsFromFile.forEach { println(it) }

    writeBankAccountsToCsv("bankData.csv", accounts)
    println("CSV values")
    println(readBankAccountsFromCsv("bankData.csv"))
}