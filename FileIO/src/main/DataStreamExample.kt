import java.io.*

fun writeBankData(fileName: String, accounts: ArrayList<BankAccount>) {
    DataOutputStream(FileOutputStream(fileName, true)).use { stream ->
        for (a in accounts) {
            stream.writeInt(a.accountNumber)
            stream.writeUTF(a.owner)
            stream.writeDouble(a.balance)
            stream.writeFloat(a.interestRate)
        }
    }
}

fun readBankData(fileName : String): ArrayList<BankAccount> {
    val accounts = ArrayList<BankAccount>()
    try {
        DataInputStream(FileInputStream(fileName)).use { stream ->
            while (true) {
                val nbr = stream.readInt()
                val owner = stream.readUTF()
                val balance = stream.readDouble()
                val interest = stream.readFloat()
                accounts.add(BankAccount(nbr, owner, balance, interest))
            }
        }
    } catch (e: EOFException) {
        println("In the file are " + accounts.size + " bankAccounts")
    }
    return accounts
}