import java.io.*

/**
 * Character streams
 * Do not use readlines() and readText() for large files! Limit of 2GB!
 */
fun copyTextFile(file1: String, file2: String): Unit {
    val inFile = FileReader(file1) // convenience class for inputstreamreader
    val outFile = FileWriter(file2, true)
    var c = inFile.read()
    while (c != -1) {
        outFile.write(c)
        c = inFile.read()
    }

    // alternatively use functional style
    // Good even for huge files
    FileReader(file1).use { reader ->
        reader.forEachLine {
            outFile.write(it)
        }
    }

    inFile.close()
    outFile.close()
}

/**
 * PrintWriter
 */
fun printWriter(fileName: String) {
    val out = PrintWriter("output.txt")
    out.println(29.95)
    out.println("Hello, World!")
    out.print("Without newline")
    out.printf("%d", 2)
    out.close()
}

/**
 * Character stream with encoding
 */
fun convert(fileName1: String, fileName2: String) {
    println("Default encoding is")
    print(System.getProperty("file.encoding"))
    val text = InputStreamReader(FileInputStream(fileName1), "cp1252").readText()
    OutputStreamWriter(FileOutputStream(fileName2), "UTF-8").use { writer ->
        writer.write(text)
    }
}