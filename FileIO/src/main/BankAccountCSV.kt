import java.io.FileReader
import java.io.FileWriter

fun writeBankAccountsToCsv(fileName: String, bankAccounts: ArrayList<BankAccount>){
    FileWriter(fileName).use { fw ->
        fw.write("AccountNr")
        fw.write(",")
        fw.write("Owner")
        fw.write(",")
        fw.write("Balance")
        fw.write(",")
        fw.write("InterestRate")
        fw.write("\n")
        bankAccounts.forEach {
            fw.write(it.accountNumber.toString())
            fw.write(",")
            fw.write(it.owner)
            fw.write(",")
            fw.write(it.balance.toString())
            fw.write(",")
            fw.write(it.interestRate.toString())
            fw.write("\n")
        }
    }
}

fun readBankAccountsFromCsv(fileName : String) : ArrayList<BankAccount> {
    val retVal = ArrayList<BankAccount>()
    FileReader(fileName).use { fr ->
        var isFirstLine = true
        fr.forEachLine { l ->
            if (isFirstLine) {
                isFirstLine = false
            } else {
                val cells = l.split(',')
                if (cells.count() > 3) {
                    retVal.add(BankAccount(
                            cells[0].toInt(),
                            cells[1],
                            cells[2].toDouble(),
                            cells[3].toFloat()))
                }
            }
        }
    }

    return retVal
}