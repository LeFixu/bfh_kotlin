import java.io.FileInputStream
import java.io.FileOutputStream

/**
 * Byte stream
 */
fun copyFile(file1: String, file2: String): Unit {
    val inFile = FileInputStream(file1)
    val outFile = FileOutputStream(file2) // override file if existing, otherwise create new
//    val outFile = FileOutputStream(file2, true) // true = append flag
    var c = inFile.read()
    while (c != -1) {
        outFile.write(c) // write 1 byte
        c = inFile.read() // read 1 byte
    }
    inFile.close()
    outFile.close()

    // alternatively use
    inFile.copyTo(outFile)
    var allbytes = inFile.readBytes()
}