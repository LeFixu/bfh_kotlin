import kotlin.math.sqrt

fun main() {
    smallExercise()
    smallExerciseContinued()
    fibonnaciSequence().take(5).forEach { println(it) }
}

fun getSmallExerciseSequence() : Sequence<Int> {
    return generateSequence { (0..9).random() }
            .take(100)
            .filter{it != 0}
}

fun smallExercise() {
    println("Average: " + getSmallExerciseSequence().average())
    println("Amount of 7: " + getSmallExerciseSequence().count{it == 7})
    println("All numbers are between 0 and 9: " + getSmallExerciseSequence().all{it in (0..9)})
}

fun smallExerciseContinued() {
    println("Number occurence: " + getSmallExerciseSequence().groupBy { it }.map{Pair(it.key, it.value.count())}.sortedBy { it.first })
    println("Result: " + getSmallExerciseSequence().distinct().map{it*it}.joinToString())
    println("3.)" + getSmallExerciseSequence().distinct().filter { it != 0}.reduce { accumulated, current -> accumulated * current })
}

fun isPrime(n : Long) : Boolean = (n > 1 && (2..sqrt(n.toDouble()).toLong()).none { n % it == 0L})
fun fibonnaciSequence() = sequence {
    var i: Long = 0
    while (true) {
        i++
        if (isPrime(i)) {
            yield(i) // returns the value to the caller
// and then continue executing the next line of code.
        }
    }
}

fun createSequenceFromArray() : List<Int>{
    return arrayOf(1, 2, 3, 4, 5, 6)
            .asSequence()
            .filter { it % 2 == 1 }
            .toList()
}

fun createSequenceFromRange() : List<Int>{
    return (1..6)
            .asSequence()
            .filter { it % 2 == 1 }
            .toList()
}

fun createSequence() : List<Int>{
    return sequenceOf(1, 2, 3, 4, 5)
            .filter { it % 2 == 1 }
            .toList()
}

fun createSequenceFromGenerator() : List<Int>{
    return generateSequence(1) { it + 1 }
            .take(5)
            .filter { it % 2 == 1 }
            .toList()
}

fun pipeline(ints : List<Int>) : List<Int> {
    return ints.asSequence() // source
            .filter { it in (5..12) } // intermediate operation
            .map { it * it } // intermediate operation
                .toList() // terminal operation

    /*
    * Possible terminal operations:
    * toList()
    * count() / {}
    * min(), minBy(), max(), average(), sum() ...
    * foreach{}
    * any() / {}, all{}
    * joinToString(separator){}
    * reduce{}, reduceIndexed{}
    * */
}